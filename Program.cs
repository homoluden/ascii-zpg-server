﻿using Zpg.Render;
using Zpg.World;
using Zpg.World.DTO.Commands;

var abort = false;

var rnd = new Random();

var mapName = "map1";
var localDir = Directory.GetCurrentDirectory();
var mapDefPath = Path.Combine(localDir, "Maps", $"{mapName}.json");
await Map.Instance.Init(mapDefPath);

Will.Instance.Init();

var inputLoop = Task.Run(async () => {
    while (!abort)
    {
        var inp = Console.ReadKey(true);
        if (inp.Key == ConsoleKey.Q)
        {
            abort = true;
        } else if (inp.Key == ConsoleKey.W)
        {
            var newCommands = Will.Instance.Avatars
                .Select(a => new RandomWalk{ AvatarName = a.Name, Code = Codes.RandomWalk, Duration = 1 + rnd.Next(10) })
                .Cast<IAvatarCommand>()
                .ToArray();
            Will.Instance.Command(newCommands);
        }

        await Task.Delay(100);
    }
});

var gameLoop = Task.Run(async () => {
    Console.Clear();

    // Draw Window (initial)
    var pvw = Console.WindowWidth;
    var pvh = Console.WindowHeight;
    
    IRenderable screenPresenter = new ScreenPresenter();
    IRenderable footer = new Footer();
    IRenderable avatars = new AvatarRenderer();
    IRenderable window = new GameWindow(screenPresenter, new NoOp(), footer, avatars);
    await window.RenderAsync(0, 0, pvw, pvh);

    while(!abort) {
        // Get current Console size
        var vw = Console.WindowWidth;
        var vh = Console.WindowHeight;
        
        await window.RenderAsync(0, 0, vw, vh-1);
        Console.SetCursorPosition(0, vh - 1);
        
        await Task.Delay(300);
    }
});

var willLoop = Task.Run(async () => {
    while (!abort)
    {    
        Will.Instance.Update();

        await Task.Delay(350);
    }
});

await Task.WhenAll(inputLoop, gameLoop, willLoop);
