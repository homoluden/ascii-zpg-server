using Zpg.World.DTO;
using Zpg.World.DTO.Commands;

namespace Zpg.World
{
    public sealed class Will
    {
        private static readonly Lazy<Will> _lazy = new Lazy<Will>(() => new Will());

        private Random _rnd = new Random();

        public AvatarState[] Avatars = Array.Empty<AvatarState>();

        public List<IAvatarCommand> ActiveCommands = new List<IAvatarCommand>();

        public static Will Instance { get { return _lazy.Value; } }

        private Will() { }

        public void Init() {
            var spawners = Map.Instance.AvatarSpawners;

            Avatars = new AvatarState[spawners.Length];
            for (int i = 0; i < spawners.Length; i++)
            {
                Avatars[i] = new AvatarState {
                    Name = $"Ava{i}",
                    Position = new Position(spawners[i].Position.X, spawners[i].Position.Y)
                };
            }
        }

        public void Command(IAvatarCommand[] commands) {
            ActiveCommands.AddRange(commands.Where(c => !ActiveCommands.Any(ac => ac.AvatarName == c.AvatarName)));
        }

        public void Update() {
            foreach (var cmd in ActiveCommands.Select(c => c).ToArray())
            {
                HandleCommand(cmd);
            }
        }

        private void HandleCommand(IAvatarCommand cmd)
        {
            var avatar = Avatars.SingleOrDefault(a => a.Name == cmd.AvatarName) ?? AvatarState.Empty;
            if (avatar == AvatarState.Empty || cmd.Duration <= 0)
            {
                ActiveCommands.Remove(cmd);
                return;
            }

            switch (cmd.Code)
            {
                case Codes.Idle: /* TODO: some idle animation */ break;
                case Codes.RandomWalk: 
                    DoRandomWalk(avatar);
                    cmd.Duration--;
                break;
                default: ActiveCommands.Remove(cmd); break;
            }
        }

        private AvatarState DoRandomWalk(AvatarState avatar)
        {
            var dir = _rnd.Next(8); // 0 - Up; 1 - NE; 2 - E; 3 - SE; 4 - S ...
            return Step(dir, avatar);
        }

        private AvatarState Step(int dir, AvatarState avatar)
        {
            var curPos = avatar.Position;
            switch (dir)
            {
                case 0: // Up
                    avatar.Position += new Position(0, -1);
                break;
                case 1: // NE
                    avatar.Position += new Position(1, -1);
                break;
                case 2: // E
                    avatar.Position += new Position(1, 0);
                break;
                case 3: // SE
                    avatar.Position += new Position(1, 1);
                break;
                case 4: // S
                    avatar.Position += new Position(0, 1);
                break;
                case 5: // SW
                    avatar.Position += new Position(-1, 1);
                break;
                case 6: // W
                    avatar.Position += new Position(-1, 0);
                break;
                case 7: // NW
                    avatar.Position += new Position(-1, -1);
                break;
                default: break;
            }

            return RespawnIfEscaped(avatar);
        }

        private AvatarState RespawnIfEscaped(AvatarState avatar)
        {
            var mapCenter = Map.Instance.CenterPosition;
            var avaPos = avatar.Position;

            if (Math.Abs(avaPos.X) >= mapCenter.X || Math.Abs(avaPos.Y) >= mapCenter.Y)
            {
                var emptySpawners = Map.Instance.AvatarSpawners.Where(s => !Avatars.Any(a => a.Position == s.Position)).ToArray();
                var spawnIdx = _rnd.Next(emptySpawners.Length);
                var spawner = emptySpawners[spawnIdx];
                avatar.Position = spawner.Position;
            }

            return avatar;
        }
    }
}