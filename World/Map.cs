using Newtonsoft.Json;
using Zpg.World.DTO;

namespace Zpg.World
{
    public sealed class Map
    {
        private static readonly Lazy<Map> _lazy = new Lazy<Map>(() => new Map());
        private bool _initialized = false;
        private char[] _mapTiles = Array.Empty<char>();

        public static Map Instance { get { return _lazy.Value; } }

        public Position CenterPosition { get; private set; } = new Position();

        public Position Size { get; private set; } = default(Position);
        public StaticObject[] StaticObjects { get; private set; } = Array.Empty<StaticObject>();
        public AvatarSpawner[] AvatarSpawners { get; private set; } = Array.Empty<AvatarSpawner>();
        public PlantSpawner[] PlantSpawners { get; private set; } = Array.Empty<PlantSpawner>();

        private Map() { }

        public async Task Init(string mapDefPath) {
            var mapDefText = await File.ReadAllTextAsync(mapDefPath);
            var mapDef = await Task.Run(() => JsonConvert.DeserializeObject<MapDef>(mapDefText));

            this.Size = mapDef.MapSize;
            this.CenterPosition = new Position((int)Math.Floor(Size.X * 0.5), (int)Math.Floor(Size.Y * 0.5));
            this.StaticObjects = mapDef.MapStaticObjects;
            this.AvatarSpawners = mapDef.AvatarSpawners;
            this.PlantSpawners = mapDef.PlantSpawners;

            InitMapTiles();

            _initialized = true;
        }

        public char GetTile(Position atPos) {
            if (!_initialized)
            {
                return '░';
            }

            var relPos = atPos + CenterPosition;

            if (relPos.X < 0 || relPos.X >= Size.X || relPos.Y < 0 || relPos.Y >= Size.Y)
            {
                return '░';
            }

            var tile = _mapTiles[relPos.Y * Size.X + relPos.X];
            
            return tile;
        }

        private void InitMapTiles()
        {
            _mapTiles = new char[Size.X * Size.Y];
            for (int i = 0; i < Size.Y; i++)
            {
                for (int j = 0; j < Size.X; j++)
                {
                    if (i == 0 || i == Size.Y - 1 || j == 0 || j == Size.X - 1)
                    {
                        _mapTiles[i * Size.X + j] = '▫';
                        continue;
                    }

                    _mapTiles[i * Size.X + j] = ' ';
                }
            }

            foreach (var staticObj in StaticObjects)
            {
                var sp = staticObj.Position + CenterPosition;
                _mapTiles[sp.Y * Size.X + sp.X] = staticObj.Type;
            }

            foreach (var spawner in AvatarSpawners)
            {
                var sp = spawner.Position + CenterPosition;
                _mapTiles[sp.Y * Size.X + sp.X] = '◎';
            }
        }
    }
}