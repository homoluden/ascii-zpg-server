using Newtonsoft.Json;

namespace Zpg.World.DTO
{
    public struct MapDef {
        [JsonProperty("size")]
        public Position MapSize { get; set; }

        [JsonProperty("staticObjects")]
        public StaticObject[] MapStaticObjects { get; set; }

        [JsonProperty("avatarSpawners")]
        public AvatarSpawner[] AvatarSpawners { get; set; }

        [JsonProperty("plantSpawners")]
        public PlantSpawner[] PlantSpawners { get; set; }
    }
}