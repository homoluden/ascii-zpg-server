using Newtonsoft.Json;

namespace Zpg.World.DTO
{
    public struct PlantSpawner
    {
        [JsonProperty("k")]
        public string Kind { get; set; }

        [JsonProperty("v")]
        public string Variant { get; set; }

        [JsonProperty("p")]
        public Position Position { get; set; }
    }
}