using Newtonsoft.Json;

namespace Zpg.World.DTO
{
    public struct StaticObject
    {
        [JsonProperty("t")]
        public char Type { get; set; }

        [JsonProperty("p")]
        public Position Position { get; set; }
    }
}