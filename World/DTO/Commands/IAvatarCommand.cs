namespace Zpg.World.DTO.Commands
{
    public interface IAvatarCommand {
        Codes Code { get; }

        int Duration { get; set; }

        string AvatarName { get; }
    }
}