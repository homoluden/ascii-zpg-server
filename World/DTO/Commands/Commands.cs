using Newtonsoft.Json;

namespace Zpg.World.DTO.Commands
{
    public struct RandomWalk : IAvatarCommand
    {
        public RandomWalk() { }
        
        public Codes Code { get; set; } = Codes.RandomWalk;

        public int Duration { get; set; } = 3;

        public string AvatarName { get; set; } = string.Empty;
    }
}