using Newtonsoft.Json;

namespace Zpg.World.DTO
{
    public class AvatarState
    {
        public static AvatarState Empty = new AvatarState {};

        [JsonProperty("name")]
        public string Name { get; set; } = string.Empty;

        [JsonProperty("p")]
        public Position Position { get; set; } = default(Position);

        public static bool operator ==(AvatarState a, AvatarState b) => (string.IsNullOrWhiteSpace(a.Name) && string.IsNullOrWhiteSpace(b.Name)) || a.Name == b.Name;
        public static bool operator !=(AvatarState a, AvatarState b) => 
            (string.IsNullOrWhiteSpace(a.Name) && !string.IsNullOrWhiteSpace(b.Name))
            || (!string.IsNullOrWhiteSpace(a.Name) && string.IsNullOrWhiteSpace(b.Name))
            || a.Name == b.Name;
    }
}