using Newtonsoft.Json;

namespace Zpg.World.DTO
{
    public struct AvatarSpawner
    {
        [JsonProperty("p")]
        public Position Position { get; set; }
    }
}