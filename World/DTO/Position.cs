using Newtonsoft.Json;

namespace Zpg.World.DTO
{
    public struct Position {
        public Position(int x, int y)
        {
            X = x;
            Y = y;
        }
        
        [JsonProperty("x")]
        public int X { get; }
        
        [JsonProperty("y")]
        public int Y { get; }

        public static Position operator +(Position a, Position b) => new Position(a.X + b.X, a.Y + b.Y);
        public static Position operator -(Position a, Position b) => new Position(a.X - b.X, a.Y - b.Y);

        public static bool operator ==(Position a, Position b) => a.X == b.X && a.Y == b.Y;
        public static bool operator !=(Position a, Position b) => a.X != b.X || a.Y != b.Y;

        public override string ToString()
        {
            return $"[{X}; {Y}]";
        }
    }
}