public interface IRenderable
{
    Task RenderAsync(int left, int top, int width, int height);
}