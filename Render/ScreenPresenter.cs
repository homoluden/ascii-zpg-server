namespace Zpg.Render
{
    public class ScreenPresenter : RenderableBase, IRenderable {
        private MapRenderer _map = new MapRenderer();

        public override async Task RenderAsync(int left, int top, int width, int height) {
            await base.RenderAsync(left, top, width, height);
            await _map.RenderAsync(left, top, width - 2, height - 2);
        }
    }
}