namespace Zpg.Render {
    public class Footer : RenderableBase, IRenderable
    {
        private byte _i = 0;

        private char[] _heartBeats = new [] {
            '-',
            '\\',
            '|',
            '/'
        };

        public override async Task RenderAsync(int left, int top, int width, int height)
        {
            await base.RenderAsync(left, top, width, height);
            await RenderSelfAsync();
        }

        private async Task RenderSelfAsync() {
            await Task.Run(() => {
                Console.SetCursorPosition(Left, Top);
                Console.Write("q: Exit | w: Random Walk all Avatars");
                Console.SetCursorPosition(Left + Width - 2, Top);
                Console.Write(_heartBeats[_i]);
                
                _i++;
                if (_i == 4) _i = 0;
            });
        }
    }
}