public class RenderableBase : IRenderable
{
    public int Left {get; private set; } = 0;
    public int Top {get; private set; } = 0;
    public int Width {get; private set; } = 0;
    public int Height {get; private set; } = 0;

    public virtual Task RenderAsync(int left, int top, int width, int height)
    {
        Left = left;
        Top = top;
        Width = width;
        Height = height;

        return Task.CompletedTask;
    }
}
