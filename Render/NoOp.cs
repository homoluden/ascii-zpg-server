namespace Zpg.Render {
    public class NoOp : IRenderable
    {
        public async Task RenderAsync(int left, int top, int width, int height)
        {
            await Task.CompletedTask;
        }
    }
}