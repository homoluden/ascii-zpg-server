using Zpg.World;
using Zpg.World.DTO;

namespace Zpg.Render
{
    public class AvatarRenderer : IRenderable {
        private char _avatarChar = '⍾';

        public async Task RenderAsync(int left, int top, int width, int height) {
            var screenCenter = new Position((int)Math.Floor(width * 0.5), (int)Math.Floor(height * 0.5));

            await Task.Run(() => {
                var avatars = Will.Instance.Avatars;

                foreach (var ava in avatars)
                {
                    // Map Center is aligned with Screen Center
                    var p = ava.Position + screenCenter;
                    Console.SetCursorPosition(p.X, p.Y);
                    Console.Write($"{_avatarChar} {ava.Name}");
                }
            });
        }
    }
}