namespace Zpg.Render {
    public class Border : IRenderable
    {
        private char _lt = '┌';
        private char _rt = '┐';
        private char _lb = '└';
        private char _rb = '┘';
        private char _hor = '─';
        private char _ver = '│';
        private char _ml = '├';
        private char _mr = '┤';
        private char _mt = '┬';
        private char _mb = '┴';

        public CornersConfiguration CornersConfig { get; }

        public Border(CornersConfiguration cornersConfig)
        {
            CornersConfig = cornersConfig;
        }

        public async Task RenderAsync(int left, int top, int width, int height)
        {
            await Task.Run(() => {
                Console.SetCursorPosition(left, top);
                Console.Write(_lt);

                for (int i = 1; i < width - 1; i++)
                {
                    Console.Write(_hor);
                }

                Console.Write(_rt);

                for (int i = 1; i < height - 1; i++)
                {
                    Console.SetCursorPosition(left, top + i);
                    Console.Write(_ver);
                    Console.SetCursorPosition(left + width - 1, top + i);
                    Console.Write(_ver);
                }

                Console.SetCursorPosition(left, top + height - 1);
                Console.Write(_lb);

                for (int i = 1; i < width - 1; i++)
                {
                    Console.Write(_hor);
                }

                Console.Write(_rb);

                RenderCorners(left, top, width, height, CornersConfig);
            });
        }

        private void RenderCorners(int left, int top, int width, int height, CornersConfiguration cornersConfig)
        {
            var ltChar = cornersConfig.LeftTop switch {
                CornerVariant.MergeWithLeft => _ml,
                CornerVariant.MergeWithTop => _mt,
                _ => _lt
            };
                
            var rtChar = cornersConfig.RightTop switch {
                CornerVariant.MergeWithRight => _mr,
                CornerVariant.MergeWithTop => _mt,
                _ => _rt
            }; 

            var lbChar = cornersConfig.LeftBottom switch {
                CornerVariant.MergeWithLeft => _ml,
                CornerVariant.MergeWithBottom => _mb,
                _ => _lb
            };

            var rbChar = cornersConfig.RightBottom switch {
                CornerVariant.MergeWithRight => _mr,
                CornerVariant.MergeWithBottom => _mb,
                _ => _rb
            };

            Console.SetCursorPosition(left, top); Console.Write(ltChar);
            Console.SetCursorPosition(left + width - 1, top); Console.Write(rtChar);
            Console.SetCursorPosition(left, top + height - 1); Console.Write(lbChar);
            Console.SetCursorPosition(left + width - 1, top + height - 1); Console.Write(rbChar);
        }
    }

    public struct CornersConfiguration {
        public CornersConfiguration() { }

        public CornerVariant LeftTop = CornerVariant.Default;
        public CornerVariant RightTop = CornerVariant.Default;
        public CornerVariant LeftBottom = CornerVariant.Default;
        public CornerVariant RightBottom = CornerVariant.Default;
    }

    public enum CornerVariant {
        Default,
        MergeWithLeft,
        MergeWithTop,
        MergeWithRight,
        MergeWithBottom
    }
}