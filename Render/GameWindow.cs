namespace Zpg.Render
{
    public class GameWindow : RenderableBase, IRenderable
    {
        private const int SIDEBAR_WIDTH = 32;
        private const int FOOTER_HEIGHT = 1;

        IRenderable _winBorder = new Border(new CornersConfiguration());
        IRenderable _footBorder = new Border(new CornersConfiguration{ LeftTop = CornerVariant.MergeWithLeft, RightTop = CornerVariant.MergeWithRight });
        IRenderable _sidebarBorder = new Border(
            new CornersConfiguration { 
                LeftTop = CornerVariant.MergeWithTop, 
                LeftBottom = CornerVariant.MergeWithBottom, 
                RightBottom = CornerVariant.MergeWithRight 
            }
        );
        
        IRenderable _gameScreen;
        IRenderable _sidebar;
        IRenderable _footer;
        IRenderable _avatarRenderer;

        public GameWindow(IRenderable gameScreen, IRenderable sidebar, IRenderable footer, IRenderable avatarRenderer)
        {
            _gameScreen = gameScreen;
            _sidebar = sidebar;
            _footer = footer;
            _avatarRenderer = avatarRenderer;
        }

        public override async Task RenderAsync(int left, int top, int width, int height)
        {
            if (left != this.Left || top != this.Top || width != this.Width || height != this.Height) {
                Console.Clear();
                await base.RenderAsync(left, top, width, height);
                await RenderSelfAsync();
            }

            await _gameScreen.RenderAsync(Left + 1, Top + 1, Width - SIDEBAR_WIDTH - 1, Height - FOOTER_HEIGHT - 2);
            await _sidebar.RenderAsync(Width - SIDEBAR_WIDTH - 2, Top + 1, SIDEBAR_WIDTH, Height - FOOTER_HEIGHT - 2);
            await _footer.RenderAsync(Left + 1, Height - FOOTER_HEIGHT - 1, Width - 2, FOOTER_HEIGHT);
            await _avatarRenderer.RenderAsync(Left + 1, Top + 1, Width - SIDEBAR_WIDTH - 1, Height - FOOTER_HEIGHT - 2);
        }

        private async Task RenderSelfAsync() {
            await Task.Run(async () => {
                await _winBorder.RenderAsync(Left, Top, Width, Height);
                await _footBorder.RenderAsync(Left, Top + Height - 3, Width, 3);
                await _sidebarBorder.RenderAsync(Left + Width - SIDEBAR_WIDTH - 2, Top, SIDEBAR_WIDTH + 2, Height - FOOTER_HEIGHT - 1);

                var initWorldTitle = $" ZPG Rougelike ({Width} x {Height}) ";
                Console.SetCursorPosition((int)Math.Ceiling(Width * 0.5 - (initWorldTitle.Length * 0.5)), 0);
                Console.Write(initWorldTitle);
            });
        }
    }
}