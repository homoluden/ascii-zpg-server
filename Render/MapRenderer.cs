using Zpg.World;
using Zpg.World.DTO;

namespace Zpg.Render
{
    public class MapRenderer : RenderableBase, IRenderable {
        public override async Task RenderAsync(int left, int top, int width, int height) {
            if (left != Left || top != Top || width != Width || height != Height)
            {
                ClearMap();
            }

            await base.RenderAsync(left, top, width, height);

            var screenCenter = new Position((int)Math.Round(Width * 0.5, 0), (int)Math.Round(Height * 0.5, 0));

            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    var screenPos = new Position(j, i);
                    var tile = Map.Instance.GetTile(screenPos - screenCenter);
                    if (tile != '░')
                    {    
                        Console.SetCursorPosition(Left + j, Top + i);
                        Console.Write(tile);
                    }
                }
            }
        }

        private void ClearMap()
        {
            for (int i = 0; i < Height; i++)
            {
                Console.SetCursorPosition(Left, Top + i);
                Console.Write(new string('░', Width));
            }
        }
    }
}